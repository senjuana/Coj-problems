#include <iostream>
using namespace std;

int main(){
	long n1, n2, n3;
	while(1)
	{
        cin >> n1 >> n2 >> n3;
        if(n1==0 || n2==0 || n3==0 )
		{
            break;
		}
		n1 *= n1;
		n2 *= n2;
		n3 *= n3;

		if( (n1+n2) == n3 )
			cout << "right" << endl;
		else if( (n2+n3) == n1 )
			cout << "right" << endl;
		else if( (n1+n3) == n2 )
			cout << "right" << endl;
		else
			cout << "wrong" << endl;
	}
}
