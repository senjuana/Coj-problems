#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

int main(int argc, char const *argv[]) {
  string t;
  std::cin >> t;
  sort(t.begin(),t.end());
  std::cout << t << std::endl;
  return 0;
}