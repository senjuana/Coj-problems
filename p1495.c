#include <stdio.h>
#include <stdlib.h>

int compare (const void * a, const void * b){
  return ( *(int*)a - *(int*)b );
}

int main (){
  int i,n;
  scanf ("%d",&n);
  int values[n]; 

  for (i=0; i<n; i++)
      scanf("%d",&values[i]);

  qsort (values, n, sizeof(int), compare);

  for (i=0; i<n; i++)
      printf ("%d\n",values[i]);

  return 0;
}