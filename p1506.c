#include <stdio.h>
int main(int argc, char const *argv[]) {
  int n,m ,i;
  char res[100],exam[100];
  double score;

  scanf("%d",&n);
  scanf("%s",res);
  scanf("%d",&m);

  while(m--){
    scanf("%s",exam);
    score = 0.0;
    for (i = 0; i < n; i++) {
      if(exam[i]==res[i])
        score += 1;
      else if(exam[i]!='#')
        score -= 0.25;
    }
    printf("%0.2lf\n",score);

  }
  return 0;
}
