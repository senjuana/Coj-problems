#include <iostream>
using namespace std;

int main(){
	int N, i;
	unsigned long long int sqrs[501]={0};

	for(i=1;i<=500;i++)
		sqrs[i] = sqrs[i-1]+i*i;

	while(cin >> N){
		if(!N) break;
		cout << sqrs[N] << endl;
	}
}