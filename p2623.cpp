#include <iostream>
#include <algorithm>
using namespace std;

int main()
{
    string cadena;
    int tam;

    cin >> cadena;
    tam = cadena.size();

    if(tam % 2 == 0){
        reverse(cadena.begin(), cadena.begin() + tam/2);
        reverse(cadena.begin() + tam/2, cadena.end());
    }
    else {
        reverse(cadena.begin(), cadena.begin() + tam/2);
        reverse(cadena.begin() + tam/2 + 1, cadena.end());
    }
    cout << cadena << "\n";
    return 0;
}