#include <iostream>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;

int sum(char *usr,char *alfa,int *val);
void limpiar( char *usr);
void llenar(char *usr, string acomodado);

int main(int argc, char const *argv[]) {
  char alfa[26]={'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
  int val[26]={1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26};
  char entrada[26];
  string ent;

  cin>>entrada;
  ent=entrada;
  sort(ent.begin(),ent.end());

  //cout<< ent<<endl;

  limpiar(entrada);
  //cout<<entrada<<endl;
  llenar(entrada,ent);
  //cout<<entrada<<endl;



  std::cout << sum(entrada,alfa,val)<< std::endl;

  return 0;
}


int sum(char *usr, char *alfa, int *val){
  int sum=0;
  for(int j=0;j<26;j++){
    for (size_t i = 0; i < 26; i++) {
        if(alfa[j]== usr[i])
        sum +=val[j];
    }
  }
  return sum;
}

void limpiar(char *usr){
    for(int i =0;i<26;i++){
        usr[i]=' ';
    }
}

void llenar(char *usr,string acomodado){
    for(int i=0;i<acomodado.length();i++){
        usr[i]=acomodado[i];
    }
}